import os
import subprocess
import datetime

# 添加文件
subprocess.run(["git", "add", "."])

# 提交文件
current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
subprocess.run(["git", "commit", "-m", f"获取当前时间：{current_time}"])

# 推送到远程仓库
subprocess.run(["git", "push"])

print("Upload successfully")