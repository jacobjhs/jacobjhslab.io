---
title: cf-worker 绑定域名
date: 2023-10-16    
tags:
- 技术
---
## cf-worker 绑定域名 实现v2ray
域名到期后，绑定自动就消失了，只要在cloudflare的绑定上新域名后，进入Workers理由后，点击“添加路由” ，在弹出的对话框里，路由填写“v2cf.yuming.com”,“yuming.com”改成自己的域名，v2cf是你的Workers名字，下面的workers有个下拉列表框，选你的Workers名字，如“v2cf”,点击“保存”即可。

## 如果是新用户
### 部署代码地址
```
部署代码：https://github.com/zizifn/edgetunnel/blob/main/src/worker-vless.js
```
### uuid生成
```
uuid生成：https://1024tools.com/uuid
```
### workers win专用ip优选
下载地址


### cf ip 优选  
```
cf ip优选；https://stock.hostmonit.com/CloudFlareYes
```

### 上面这个在线的我觉得最稳定，选出来的IP比自己测出来的要快